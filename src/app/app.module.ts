import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './partials/footer/footer.component';
import { HeaderComponent } from './partials/header/header.component';
import { SidebarComponent } from './partials/sidebar/sidebar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PaymentDetailsComponent } from './components/payment-details/payment-details.component';
import { RideHistoryComponent } from './components/ride-history/ride-history.component';
import { ViewComponent } from './components/user/view/view.component';
import { EditComponent } from './components/user/edit/edit.component';
import { AddComponent } from './components/user/add/add.component';
import { PasswordComponent } from './components/user/password/password.component';
import { HistoryComponent } from './components/user/history/history.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { DatatableDirective } from './directives/datatable.directive';
import {ViewComponent as providerViewComponent} from './components/provider/view/view.component';
import {EditComponent as providerEditComponent} from './components/provider/edit/edit.component';
import {ListComponent} from './components/provider/list/list.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { AuthService } from './services/auth.service';
import { GuestGuard } from './guards/guest.guard';
import { AuthGuard } from './guards/auth.guard';
import { TokenInterceptor } from './interceptors/token';



@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    PaymentDetailsComponent,
    RideHistoryComponent,
    ViewComponent,
    EditComponent,
    AddComponent,
    PasswordComponent,
    HistoryComponent,
    LoginComponent,
    HomeComponent,
    DatatableDirective,
    providerViewComponent,
    providerEditComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReCaptchaModule
  ],
  providers: [
    AuthService,
    GuestGuard,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
