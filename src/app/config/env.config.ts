const _isDev = window.location.port.indexOf('4200') > -1;

const getHost = () => {
    const protocol = window.location.protocol;
    const host = window.location.host;
    return _isDev ? `http://localhost:3000/api` : `${protocol}//${host}/api`;
};

const TEST_NET = true;
const apiURI =  'http://142.93.218.15/api/v1/';
console.log(apiURI);
export const ENV = {
    BASE_URI: apiURI,
    SUCCESS_STATUS: 'SUCCESS',
    LOGIN: apiURI + 'admin/login',
    ADMIN_UPDATE_PROFILE: apiURI + 'admin/profile',
    USER_API: apiURI + 'admin/crud/User',
    USER_REMOVE_API: apiURI + 'admin/crud/User/remove',
};
