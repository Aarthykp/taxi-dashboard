import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ViewComponent } from './components/user/view/view.component';
import { AddComponent } from './components/user/add/add.component';
import { PaymentDetailsComponent } from './components/payment-details/payment-details.component';
import { PasswordComponent } from './components/user/password/password.component';
import { HistoryComponent } from './components/user/history/history.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import {ViewComponent as providerViewComponent} from './components/provider/view/view.component';
import {EditComponent as providerEditComponent} from './components/provider/edit/edit.component';
import {ListComponent} from './components/provider/list/list.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'admin',
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'user', component: ViewComponent },
      { path: 'adduser', component: AddComponent},
      { path: 'transaction', component: PaymentDetailsComponent },
      { path: 'password', component: PasswordComponent },
      { path: 'history', component: HistoryComponent },
      { path: 'login', component: LoginComponent },
      { path: 'providerview', component: providerViewComponent},
      { path: 'provideredit', component: providerEditComponent},
      { path: 'providerdetail', component: ListComponent}
    ],
    component: HomeComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
