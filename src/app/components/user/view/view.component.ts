import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ENV } from 'src/app/config/env.config';
declare var Materialize: any;
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  users: [];
  constructor(private uService: UserService) { }

  ngOnInit() {
    this.uService.getUsers({}).subscribe((res) => {
      if (res.statusText === ENV.SUCCESS_STATUS) {
        this.users = res.data.User;
        console.log(res);
      }
      Materialize.toast(res.message);
    });
  }

}
