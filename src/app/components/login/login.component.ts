import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { ENV } from 'src/app/config/env.config';
declare var Materialize: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;
  @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
  sitekey = '6LeQ24cUAAAAALKiCBiEAlW8vFKslMjP1zpruUIk';
  canSubmit = false;
  constructor(private fb: FormBuilder, private auth: AuthService, private route: Router) { }
  get email() { return this.myForm.get('email'); }
  get password() { return this.myForm.get('password'); }
  ngOnInit() {
    // console.log(this.auth.getUser());
    this.myForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }
  handleCorrectCaptcha(response: any) {
    this.canSubmit = true;
  }
  captchaExpired() {
    this.canSubmit = false;
  }
  doLogin() {
    if (!this.myForm.valid) { return; }
    this.auth.login(
      Object.assign(
        this.myForm.value,
        {
          captchaResponse: this.captcha.getResponse()
        }
      )
    ).subscribe((res) => {
      if (res.statusText === ENV.SUCCESS_STATUS) {
        this.auth.setUser(res.data);
        this.auth.setAccessToken(res.data.access_token);
        this.route.navigate(['/admin/dashboard']);
      } else {
        if (res.message === 'INVALID_CAPTCHA') { this.captcha.reset(); }
      }
      Materialize.toast(res.message, 3000);
    });
  }

}
