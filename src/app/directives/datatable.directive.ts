import { Directive, ElementRef } from '@angular/core';
declare var $: any;
@Directive({
  selector: '.info-data-table'
})
export class DatatableDirective {

  constructor(el: ElementRef) {
    setTimeout(() => {
      $(el.nativeElement).DataTable({
        "bLengthChange": false,
      });
    }, 0)
  }

}
